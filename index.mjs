import Grammar from "./grammar.mjs";
import Parser from "./parser.mjs";

export { default as Clause } from "./clause.mjs";
export { default as Grammar } from "./grammar.mjs";
export { default as Parser } from "./parser.mjs";
export { default as ParseState } from "./parse-state.mjs";
export { default as Rule } from "./rule.mjs";

/**
 * @template [ReturnType=any]
 * @callback cpegParser
 * @param {string|string[]} source
 * @param  {any[]} args 
 * @returns {ReturnType}
 */
/**
 * Template literal that parses a CPEG grammar and returns a parser object which can parse
 * the grammar.
 * 
 * @template [ReturnType=any]
 * @param {string|string[]} grammarString
 * @param {Function[]} args
 * @returns {cpegParser<ReturnType>}
 */
const cpeg = (grammarString, ...args) => {
    if (grammarString === undefined) {
        throw new Error("Expecting a grammar or invocation as a template literal via cpeg` your : grammar; `");
    }
    if ("raw" in grammarString) {
        grammarString = grammarString.raw.reduce((result, value, index) => {
            return result + value + (index in args ? '%%' + index + '%%' : '');
        }, '');
    }
    const grammar = (new Grammar.Parser()).parse(grammarString, ...args);
    /**
     * @type {Parser<ReturnType, void>}
     */
    const parser = new Parser(grammar);

    return function cpegParser(source, ...args) {
        if (typeof source === "object" && "raw" in source) {
            source = source.raw.reduce((result, value, index) => {
                return result + value + ((index in args && args[index] !== undefined) ? '%arguments[' + index + ']' : '');
            }, '');
        }
        return parser.parse(source, ...args);
    }
};

export default cpeg;
