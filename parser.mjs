import CPEGParseError from "./errors.mjs";
import Grammar from "./grammar.mjs";
import ParseState, { Trace } from "./parse-state.mjs";
import Rule from "./rule.mjs";
import Clause from "./clause.mjs";
import StringTool from "../util/stringtool.mjs";
/**
 * The CPEG parser generator creates a parser function which can be used to parse most
 * context free grammars (CFGs).
 * 
 * @template [ReturnT=any]
 * @template [SettingsT=void]
 */
export default class Parser {

    /**
     * Construct a new Parser instance. If the grammar expects additional state information,
     * this information is provided via the `extraData` parameter as an object.
     * 
     * @param {Grammar} grammar A grammar
     * @param {SettingsT} settings State information
     */
    constructor(grammar, settings=null) {
        if (!(grammar instanceof Grammar)) {
            throw "Grammar expected";
        }

        /**
         * @private
         */
        this._ = {
            grammar: grammar,
            settings: settings
        }
    }

    get grammar() {
        return this._.grammar;
    }

    /**
     * Parse the provided string and arguments
     * 
     * @param {string} source 
     * @param {any[]} args
     *  Options for the parser
     * @returns {ReturnT}
     */
    parse(source, ...args) {
        if (typeof source !== "string") {
            throw new Error("Parser.parse() expects a string source to parse");
        }
        const goal = this._.grammar.rules[0];
        const settings = Object.assign({}, this._.settings);
        const state = new ParseState(source, settings, args);
        const result = goal.parse(state);
        if (result === ParseState.MISS) {
            return this.parseWithTrace(source, ...args);
        }
        return result;
    }

    /**
     * @private
     * @param {string} source 
     * @param  {...any} args 
     */
    parseWithTrace(source, ...args) {
        const goal = this._.grammar.rules[0];
        const settings = Object.assign({}, this._.settings);
        const state = new ParseState(source, settings, args);
        state.trace = true;
        state.tree = new Trace(null, state.offset, null, state);
        state.current = state.tree;
        const result = goal.parse(state);
        if (result === ParseState.MISS) {
            state.endTrace(null, result);
            state.hasResult = true;
            state.result = ParseState.MISS;

            const debugInfo = state.errorInfo;

            let message = "";
            const expected = [];
            debugInfo.expected.forEach((expectation) => {
                const clause = expectation.clause;
//                console.log(expectation.clause); process.exit();
                const text = clause.description ? clause.description : clause.rule.name;
                if (expected.indexOf(text) < 0) {
                    expected.push(text);
                }
/*
                if (clause.description) {
                    expected.push(clause.description);
                } else {
                    expected.push(clause.rule.name);
                }
*/
            });
            if (expected.length > 0) {
                message += "Expected " + expected.join(" or ");
                if (debugInfo.offset === source.length) {
                    message += " but end of file reached";
                } else {
                    message += " but found " + debugInfo.unexpected.kind;
                }
            } else {
                message += "Unexpected " + debugInfo.unexpected.kind;
            }
            //console.log(debugInfo);
            message += " in <source:" + debugInfo.lineNumber + ":" + debugInfo.columnNumber + ">";
            const linePrefix = "line " + debugInfo.lineNumber + ": ";
            message += "\n  " + linePrefix + debugInfo.line;
            message += "\n  " + (" ".repeat(linePrefix.length)) + debugInfo.errorMarker + "--- unexpected " + JSON.stringify(debugInfo.unexpected.string);

            throw new CPEGParseError(message, {
                column: debugInfo.columnNumber,
                line: debugInfo.lineNumber,
                offset: debugInfo.offset,
                source: source
            });
        }
        return result;
    }

}