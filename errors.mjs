import StringTool from "../util/stringtool.mjs"
import Clause from "./clause.mjs";
import Rule from "./rule.mjs";

export default class CPEGParseError extends Error {

    /**
     * @param {string} errorMessage 
     * @param {({offset: number} | {line: number, column: number}) & {filename?: string, source?: string, trace?: {rule: Rule, clause: Clause, startOffset: number}[]}} locationInfo 
     */
    constructor(errorMessage, locationInfo) {

        let locationDesc;
        let st = ("source" in locationInfo) ? new StringTool(locationInfo.source) : null;
        let trace = "";
        let locationOffsets;
    
        if ("offset" in locationInfo) {
            // at least we have the offset
            locationOffsets = [ locationInfo.offset ];
        }
        if (!("line" in locationInfo) && st && "offset" in locationInfo) {
            // if no line/column is given, calculate it since we have source
            locationInfo.line = st.lineNumber(locationInfo.offset);
            locationInfo.column = st.columnNumber(locationInfo.offset);
        }
        if ("line" in locationInfo) {
            // replace the location description with a proper line/column description
            locationOffsets = [ locationInfo.line + 1 ];
            if ("column" in locationInfo) {
                locationOffsets.push(locationInfo.column + 1);
            }
        }
        if ("filename" in locationInfo) {
            // add the filename to the error location description
            locationDesc = "(" + locationInfo.filename + ":" + locationOffsets.join(":") + ")";
        } else {
            locationDesc = "(<anonymous>:" + locationOffsets.join(":") + ")";
        }
        let message = errorMessage + " in " + locationDesc;
        if ("trace" in locationInfo) {
            const traceLines = [];
            locationInfo.trace.forEach((trace, index) => {
                let line = "at " + trace.rule.name;
                if (trace.clause) {
                    line += ": " + trace.clause.toString();
                }
                line += " (" + (locationInfo.filename ?? "");
                if ("startOffset" in trace) {
                    if (st) {
                        line += ":" + (st.lineNumber(trace.startOffset) + 1) + ":" + (st.columnNumber(trace.startOffset) + 1);
                    } else {
                        line += ":" + trace.startOffset;
                    }
                }
                line += ")";
            });
            trace = "   " + traceLines.join("\n   ");
            message += "\n" + trace;
        }

        super(message);
        this.stack = trace;
    }
}