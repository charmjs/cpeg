import Clause from "./clause.mjs";
import Grammar from "./grammar.mjs";
import ParseState from "./parse-state.mjs";
import util from "util";
import debugObject from "../util/debug-object.mjs";

export default class Rule {
    /**
     * @param {string} name 
     * @param {Clause} clause 
     * @param {(result: any, state: ParseState) => any} [processor]
     */
    constructor(name, clause, processor=null) {
        if (!(clause instanceof Clause)) {
            throw new Error("Expected Clause instance, got " + clause);
        }
        this.name = name;
        this.ready = false;
        this.clause = clause;
        this.processor = processor;
        /** @type {boolean|null} */
        this.isLeftRecursive = null;
        Object.seal(this);
    }

    /**
     * @param {ParseState} state 
     */
    parse(state, ...callers) {
        /*
        if (this.clause instanceof Clause.Sequence) {
            console.log("Running Sequence", this.name);
        }
        */
        const result = this.clause.parse(state, ...callers);
        if (result !== ParseState.MISS && this.processor) {
            return (this.processor)(result, state);
        }
        return result;
    }

    /**
     * First stage of setup after a grammar is ready to begin parsing. Should
     * be used to wire up references.
     * 
     * @param {Grammar} grammar 
     */
    prepare(grammar) {
        this.clause.init(this);
        this.clause.prepare(grammar, this);
    }

    /**
     * Last stage of setup after a grammar is ready to begin parsing
     * 
     * @param {Grammar} grammar 
     */
    setup(grammar) {
        this.clause.setup(grammar, this);
        this.ready = true;
    }

    toString() {
        let str = this.name;
        if (this.clause.description) {
            str += " " + JSON.stringify(this.clause.description) + " ";
        }
        str += "\n    : " + this.clause.toString() + ";";
        return str;
    }

    [util.inspect.custom]() {
        return debugObject("Rule", {
            name: this.name,
            desc: this.clause.description,
            clause: this.clause
        })
        return [ "Rule", this.name, this.description ];
    }

}