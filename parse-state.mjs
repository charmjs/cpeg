import StringTool from "../util/stringtool.mjs";
import util from "util";
import Clause from "./clause.mjs";
import debugObject from "../util/debug-object.mjs";

/**
 * @template [OptionsT=void]
 */
export default class ParseState {
    static MISS = Symbol("FAILURE");
    static HIT = Symbol("SUCCESS");

    /**
     * @param {string} source The string to parse
     * @param {OptionsT} options Configuration options for the grammar
     * @param {any[]} args Extra arguments passed to the parse function
     */
    constructor(source, options=undefined, args) {
        /**
         * Storage space for information gathered during parsing
         * 
         * @type {[key: string|number]: any}
         */
        this.$ = {}

        /**
         * @type {boolean}
         */
        this.hasResult = false;

        /**
         * @type {any}
         */
        this.result = null;

        /**
         * Parser settings provided when the parser was constructed
         * 
         * @type {OptionsT}
         */
        this.options = options;
        Object.freeze(this.options);

        /**
         * Extra arguments passed to the `Parser.parse()` function.
         * 
         * @type {any[]}
         */
        this.arguments = args;
        Object.freeze(this.arguments);
        

        /**
         * The full source we're parsing
         * 
         * @type {string}
         */
        this.source = source;

        /**
         * The current parsing offset
         * 
         * @type {number}
         */
        this.offset = 0;

        /**
         * The length of the source we're parsing
         * 
         * @type {number}
         */
        this.length = source.length;

        /**
         * The parsing offset of the last commit token
         * 
         * @type {number}
         */
        this.committed = 0;

        /**
         * Should the parser build a full parse trace?
         * 
         * @type {boolean}
         */
        this.trace = false;

        /**
         * The full tracing tree structure
         * 
         * @type {Trace|null}
         */
        this.tree = null;

        /**
         * The current tracing node
         * 
         * @type {Trace|null}
         */
        this.current = null;
        this.traceLog = [];

        Object.seal(this);
    }

    /**
     * @param {Clause} clause 
     * @returns 
     */
    beginTrace(clause) {
        if (!this.trace) return;

        //console.log(this.offset + "  ".repeat(this.current.depth), clause.description ? clause.description : clause.toString());

        const trace = new Trace(clause, this.offset, this.current, this);
        this.current.children.push(trace);
        this.current = trace;
    }

    /**
     * @param {Clause} clause 
     * @returns 
     */
    endTrace(clause, result) {
        if (!this.trace) return;

        //console.log(this.offset + "  ".repeat(this.current.depth-1), result);

        const trace = this.current;

        // check that the endTrace matches the startTrace
        if (trace.clause !== clause) {
            throw new Error("Trace mismatch, expecting traceEnd from " + this.current.clause + " but received from " + clause);
        }

        // wrap up this trace
        trace.endOffset = this.offset;
        trace.result = result;
        trace.hasResult = true;

/*
        console.table([[clause.toString(),clause.rule.name]]);
        console.log(
            clause.toString(), "in", clause.rule.name, "@", this.current.offset, "-", this.offset, 
            JSON.stringify(this.source.substring(this.offset, this.offset + 5)),
            "result", result === ParseState.MISS ? false : (result === ParseState.HIT ? true : JSON.stringify(result)));
*/

        //console.log("  ".repeat(trace.depth-1), result !== ParseState.MISS);

        let endOffset = trace.offset + Math.min(trace.length, 10);
        this.traceLog.push({
            source: this.source.substring(trace.offset, endOffset),
            start: trace.offset,
            end: trace.endOffset,
            rule: trace.clause && trace.clause.rule ? trace.clause.rule.name : null,
            clause: trace.clause ? trace.clause.toString() : null,
            result: result === ParseState.MISS ? false : (result === ParseState.HIT ? true : result)
        });

        this.current = trace.parent;
    }

    commit() {
        if (this.offset > this.committed) {
            this.commited = this.offset;
        }
    }

    /**
     * @type {number}
     */
    get longestParse() {
        this.assertHasTrace();
        let longest = 0;
        for (const trace of this.tree.visitPreOrder()) {
            if (trace.hasResult && longest < trace.endOffset) {
                longest = trace.endOffset;
            }
        }
        return longest;
    }

    /**
     * Extract some information which can be used to write better error
     * messages: Offset that was unparseable, which clauses were attempted 
     * at that offset, which clauses failed at that offset and their call 
     * stack.
     */
    get errorInfo() {
        this.assertHasTrace();
        this.assertHasFailed();

        let errorOffset = 0;
        for (const trace of this.tree.visitPreOrder()) {
            if (!trace.clause || trace.clause.rule.clause !== trace.clause) {
                continue;
            }
            if(trace.result !== ParseState.MISS && trace.endOffset > errorOffset) {
                errorOffset = trace.endOffset;
            }
        }

        let st = new StringTool(this.source);

        const debugInfo = {
            offset: errorOffset,
            lineNumber: st.lineNumber(errorOffset),
            columnNumber: st.columnNumber(errorOffset),
            line: st.lineAt(errorOffset),
            errorMarker: " ".repeat(st.columnNumber(errorOffset)) + "^",
            unexpected: st.wordAt(errorOffset),
            expected: null,
        };

        /**
         * Find the common ancestor for all the failed parses at the errorOffset
         */

        // first find the clauses
        const allFailedTraces = this.tree.descendants.filter((trace) => trace.offset === errorOffset);

        // traces filtered by common ancestor except root
        const failedTraces = [];
        for (const trace of allFailedTraces) {
            let current = trace;
            // find the topmost trace at the error offset
            while (current.parent && current.parent.clause && current.parent.offset === errorOffset) {
                current = current.parent;
            }
            if (failedTraces.indexOf(current) < 0) {
                failedTraces.push(current);
            }
        }
        debugInfo.expected = failedTraces.map((trace) => {
            const result = {
                clause: trace.clause,
                offset: trace.offset,
                result: trace.result,
                precedingSource: this.source.substring(Math.max(0, trace.offset - 10), trace.offset),
                parent: null
            };
            let current = result;
            while(trace.parent) {
                trace = trace.parent;
                current.parent = {
                    clause: trace.clause,
                    offset: trace.offset,
                    result: trace.result,
                    precedingSource: this.source.substring(Math.max(0, trace.offset - 10), trace.offset),
                    parent: null
                };
                current = current.parent;
            }
            return result;
        });
        //console.log(util.inspect(debugInfo.expected, false, 7, true)); process.exit();
        const dump = (r, i=0) => {
            let res = " ".repeat(i) + `${r.clause ? r.clause.toString : 'null'} @ ${r.offset} => ${util.inspect(r.result)}`;
            if (r.parent) {
                res += "\n" + dump(r.parent, i+1);
            }
            return res;
        };
        /*
        for (const t of debugInfo.expected) {
            console.log("TRACE:", dump(t));
        }
        */
        //console.log("----");process.exit();
        return debugInfo;
    }

    assertHasTrace() {
        if (!this.trace) {
            throw new Error("Trace not enabled");
        }
    }

    assertHasFailed() {
        this.assertHasTrace();
        if (!this.tree.hasResult) {
            throw new Error("The parse has no result yet");
        }
        if (this.tree.result !== ParseState.MISS) {
            throw new Error("The parse did not fail");
        }
    }
}

export class Trace {

    /**
     * @param {Clause} clause 
     * @param {number} offset 
     * @param {Trace|null} parent
     * @param {ParseState} state
     */
    constructor(clause, offset, parent, state) {
        /**
         * @type {Clause}
         */
         this.clause = clause;
        /**
         * @type {number}
         */
         this.offset = offset;
        /**
         * @type {Trace|null}
         */
        this.parent = parent;

        /**
         * @type {TraceCollection}
         */
        this.children = new TraceCollection();
        this.result = null;
        /**
         * @type {boolean}
         */
        this.hasResult = false;

        /**
         * @type {number}
         */
        this.endOffset = -1;

        /**
         * @type {ParseState}
         */
        this.state = state;
        if (!state) {
            throw new Error("Trace without state")
        }
    }

    get parents() {
        const parents = new TraceCollection();;
        let current = this;
        while (null !== (current = current.parent)) {
            parents.push(current);
        }
        return parents;
    }

    get descendants() {
        const descendants = new TraceCollection();
        for (const descendant of this.visitPreOrder()) {
            if (descendant !== this) {
                descendants.push(descendant);
            }
        }
        return descendants;
    }

    /**
     * Property is true if the clause failed, but the parse made some progress when
     * trying to parse.
     * 
     * @type {boolean}
     */
    get isPartialSuccess() {
        this.assertHasResult();
        
        if (this.result !== ParseState.MISS) {
            // This parse was a full success
            return false;
        }

        for (const child of this.children) {
            if (child.hasResult && child.result !== ParseState.MISS) {
                return true;
            }
        }
        return false;
    }
        
    
    /**
     * @type {number}
     */
    get length() {
        this.assertHasResult();
        return this.endOffset - this.offset;
    }

    /**
     * @type {number}
     */
    get depth() {
        return this.parent ? 1 + this.parent.depth : 1;
    }

    /**
     * @type {number}
     */
    get longestParseLength() {
        let maxOffset = this.offset;
        for (const child of this.visitPreOrder()) {
            if (child.hasResult && child.endOffset > maxOffset) {
                maxOffset = child.endOffset;
            }
        }
        return maxOffset - this.offset;
    }

    /**
     * Visit all nodes root first, left to right
     * 
     * @return {Generator<Trace>}
     */
    *visitPreOrder() {
        yield this;
        for (const child of this.children) {
            yield* child.visitPreOrder();
        }
    }

    /**
     * Visit all nodes leafs first, left to right
     * 
     * @return {Generator<Trace>}
     */
    *visitPostOrder() {
        for (const child of this.children) {
            yield* child.visitPostOrder();
        }
        yield this;
    }

    /**
     * Visit all nodes by depth, left to right, root first.
     * 
     * @return {Generator<Trace>}
     */
    *visitRootFirst() {
        const queue = [ this ];
        while (queue.length > 0) {
            const current = queue.shift();
            yield current;
            for (const child of current) {
                queue.push(child);
            }
        }
    }

    /**
     * Visit all nodes by depth, left to right, leafs first
     * 
     * @return {Generator<Trace>}
     */
    *visitRootLast() {
        const buffer = [ this ];
        let i = 0;
        while (i in buffer) {
            const current = buffer[i++];
            for (const child of current.children.slice(0).reverse()) {
                buffer.push(child);
            }
        }
        while (i > 0) {
            yield buffer[--i];
        }
    }

    assertHasResult() {
        if (!this.hasResult) {
            throw new Error("Trace " + this.clause + " has not completed");
        }
    }

    get resultText() {
        if (!this.hasResult) {
            return "N/A";
        } else if (this.result === ParseState.MISS) {
            return "MISS";
        } else if (this.result === ParseState.HIT) {
            return "HIT";
        } else {
            const st = new StringTool(JSON.stringify(this.result));
            return st.ellipsis(10);
        }
    }

    [util.inspect.custom]() {
        return debugObject("Trace", {
            offset: this.offset,
            clause: this.clause,
            match: typeof this.result === "symbol" ? (this.result === ParseState.HIT ? ParseState.HIT : false) : this.state.source.substring(this.offset, this.endOffset),
        });
        return "Trace:" + this.clause.toString()+"@"+this.offset+"-"+this.endOffset;
        return JSON.stringify(["Trace", this.clause && this.clause.toString(), this.state.source.substring(this.offset, this.endOffset), this.offset, this.endOffset]);
    }
}

/**
 * @extends {Array<Trace>}
 */
class TraceCollection extends Array {


    /**
     * @returns {Clause[]}
     */
    get clauses() {
        return Array(...this.filter((trace) => trace.clause !== null).map((trace) => trace.clause));
    }

    /**
     * @returns {Rule[]}
     */
    get rules() {
        return Array(...this.filter((trace) => trace.clause && trace.clause.rule.clause === trace.clause).map((trace) => trace.clause.rule));
    }

    /**
     * @param {*} trace
     */
    has(child) {
        for (const trace of this.iterator()) {
            if (trace === child) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param {Trace} trace 
     * @returns 
     */
    ancestorsOf(trace) {
        const parents = trace.parents();
        return new TraceCollection(this.filter((candidate) => parents.indexOf(candidate) >= 0));
    }

    /**
     * 
     * @param {*} offset 
     * @returns 
     */
    descendantsOf(trace) {
        const descendants = trace.descendants();;
        return new TraceCollection(this.filter((candidate) => descendants.indexOf(candidate) >= 0));
    }

    endingAt(offset) {
        return this.filter((trace) => trace.endOffset === offset);
    }
    endingAfter(offset) {
        return this.filter((trace) => trace.endOffset > offset);
    }
    endingBefore(offset) {
        return this.filter((trace) => trace.endOffset < offset);
    }
    startingAt(offset) {
        return this.filter((trace) => trace.offset === offset);
    }
    startingBefore(offset) {
        return this.filter((trace) => trace.offset < offset);
    }
    startingAfter(offset) {
        return this.filter((trace) => trace.offset > offset);
    }
    resultIs(result) {
        return this.filter((trace) => trace.result === result);
    }
    resultIsNot(result) {
        return this.filter((trace) => trace.result !== result);
    }
    resultIsTypeOf(type) {
        return this.filter((trace) => type === typeof trace.result);
    }
    resultIsInstanceOf(constructor) {
        return this.filter((trace) => trace.result instanceof constructor);
    }
    lengthEQ(length) {
        return this.filter((trace) => trace.length === length);
    }
    lengthLT(length) {
        return this.filter((trace) => trace.length < length);
    }
    lengthGT(length) {
        return this.filter((trace) => trace.length > length);
    }
    lengthLTE(length) {
        return this.lengthLT(length + 1);
    }
    lengthGTE(length) {
        return this.lengthGT(length - 1);
    }
    get successful() {
        return this.filter((trace) => trace.result !== ParseState.MISS);
    }

    get failed() {
        return this.filter((trace) => trace.result === ParseState.MISS);
    }

    get partials() {
        return this.filter((trace) => trace.isPartialSuccess);
    }
}

class ParseLocation {
    /**
     * 
     * @param {Trace} trace 
     */
    constructor(trace) {

        /**
         * The trace for the parse for extracting 
         * @private
         * @type {Trace}
         */
        this._trace = trace;
    }

    /**
     * True if the parse was a complete success
     */
    get successful() {
        return this.trace.result !== ParseState.MISS;
    }

    /**
     * True if the parse was a complete failure
     */
    get failed() {
        if (this.successful) return false;
        if (this.partial) return false;
        return true;
    }

    /**
     * True if the parse was a partial success
     */
    get partial() {
        if (this.successful) return false;
        return this.trace.isPartialSuccess;
    }

    /**
     * The clause that was attempted at this location
     * @type {Clause}
     */
    get clause() {
        return this.trace.clause;
    }

    /**
     * The rule that was attempted at this location
     * @type {Rule}
     */
    get rule() {
        return this.clause.rule;
    }

    /**
     * @type {number}
     */
    get offset() {
        return this.trace.offset;
    }

    /**
     * @type {number}
     */
    get length() {
        return this.endOffset - this.length;
    }

    /**
     * @type {number}
     */
    get endOffset() {
        return this.trace.endOffset;
    }

    /**
     * @type {any}
     */
    get result() {
        return this.trace.result;
    }

    /**
     * Get the underlying Trace instance
     * 
     * @type {Trace}
     */
    get trace() {
        return this._trace;
    }

    /**
     * Get the parent parse location as seen in the trace
     * 
     * @type {ParseLocation}
     */
    get parent() {
        if (this.trace.parent) {
            return new ParseLocation(current.trace.parent);
        }
        return null;
    }

    /**
     * Get the child parse locations according to the trace
     * 
     * @type {ParseLocation[]}
     */
    get children() {
        const result = [];
        for (const child of this.trace.children) {
            result.push(new ParseLocation(child));
        }
        return result;
    }

    /**
     * Get a call stack for the trace
     */
    get stack() {
        let current = this;
        const stack = [current];
        while (null !== (current = current.parent)) {
            stack.push(current);
        }
        return stack.reverse();
    }
}