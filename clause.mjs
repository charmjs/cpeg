import Grammar from "./grammar.mjs";
import ParseState from "./parse-state.mjs";
import util from "util";
import debugObject from "../util/debug-object.mjs";
import Lazy from "../util/lazy.mjs";

/**
 * If defined, this function will be invoked instead of the parse function. This allows handling
 * of recursive clauses, by trapping parse invocations while in the left recursion special case.
 * 
 * @type {(state: ParseState, clause: Clause) => any}
 */
let parseFilter = null;

class LeftRecursionError extends Error {
    constructor(message, offset) {
        super(message + " at offset " + offset);
    }
}

class PreparedClause {
    constructor(clause, isLeftRecursive, precedence) {
        this.clause = clause;
        this.isLeftRecursive = isLeftRecursive;
        this.precedence = precedence;
    }
}

class Result {
    /**
     * @param {string} ruleName 
     * @param {*} result 
     * @param {number} precedence
     */
    constructor(ruleName, result, precedence) {
        this.ruleName = ruleName;
        this.result = result;
        this.precedence = precedence;
    }
}

/**
 * @typedef {Object} ClauseOptions
 * @property {boolean} terminal Is this a clause that decides alone success or failure?
 * @property {LazyValue<number>} returnCount Number of values that this clause returns. >1 means that the clause returns an array
 * @property {Lazy<boolean>} notProgressingParse If this clause does definitely not cause the parse offset to increase
 * @property {boolean} isMockClause This clause is not a parsing clause and serves a different purpose
 * @property {"!"|"&"|"..."|"-"|null} predicate If this clause has a predicate
 * @property {"?"|"+"|"*"|"{}"|null} operator If this clause has an operator
 * @property {(grammar: Grammar, rule: Clause) => null} prepare Hook into the prepare stage after the grammar has been built.
 * @property {(grammar: Grammar, rule: Clause) => null} setup Hook into the setup stage after the grammar has been built.
 * @property {(value: any, state: ParseState) => any} processor Function which will process the results from the clause before returning
 * @property {{[pragma: string]: any}} pragmas
 * @property {string} literal The literal string needed to create this clause in a grammar text, without predicate and operator
 * @property {string} description Hand written description of the clause
 */
export default class Clause {
    static Sequence = class Sequence extends Clause {

        /**
         * Represents a sequence of clauses to be evaluated one after another. Clauses which
         * does not provide results are not added to the result array.
         * 
         * In a grammar each clause are separated by whitespace
         * 
         * @param {Grammar} grammar 
         * @param  {...Clause} clauses 
         */
        constructor(...clauses) {
            super(
                {
                    terminal: false,
                    children: clauses,
                    literal: "(" + clauses.join(" ") + ")",
                    prepare: (grammar, rule) => {
                        for (const clause of clauses) {
                            clause.prepare(grammar, rule);
                        }
                        grammar.defer(() => {
                            for (const clause of this.children) {
                                //console.log("SEQUENCE CAPTURING FOR", this.toString(), "CLAUSE", clause.toString(), clause.returnCount);
                                this.data.capturingCount += clause.returnCount;
                                if (clause.isDestructuring) {
                                    this.data.hasDestructuringChild = true;
                                }
                            }
                            //console.log("SEQUENCE CAPTURING COUNT", this.toString(), this.data.capturingCount);
                        });
                    },
                    setup: (grammar, rule) => {
                        for (const clause of clauses) {
                            clause.setup(grammar, rule);
                        }
                    },
                },
                clauses
            );
        }

        parseFunction(state) {
            const startOffset = state.offset;
            const sequenceResult = [];
            for (const clause of this.children) {
                const result = clause.parse(state);
                if (result === ParseState.MISS) {
                    state.offset = startOffset;
                    return ParseState.MISS;
                } else if (result !== ParseState.HIT) {
                    if (this.data.destructuringChild && clause.isDestructuring) {
                        if (result instanceof Array) {
                            for (const value of result) {
                                sequenceResult.push(value);
                            }
                        } else {
                            sequenceResult.push(result);
                        }
                    } else {
                        sequenceResult.push(result);
                    }
                }
            }
            if (!this.data.destructuringChild && this.data.capturingCount === 1) {
                // This clause will always end with an array of one item, and in this case we'll just return item 0
                return sequenceResult[0];
            }
            return sequenceResult;
        }

        data = {
            /**
             * Number of results expected from children
             * 
             * @type {number}
             */
            capturingCount: 0,

            /**
             * Do we have any children which are destructuring?
             */
            hasDestructuringChild: false
        }

        /**
         * May this clause end up at this clause without making any progress?
         * 
         * @param {Clause} rule Does the left-most clause lead to a call to this rule?
         * @param {Clause[]} seen
         * @returns {boolean}
         */
        isLeftRecursive(rule, seen=[]) {
            if (seen.indexOf(this) >= 0) {
                // found left recursion which does not lead to rule
                return false;
            }
            seen.push(this);
            for (const child of this.children) {
                if (child.isMockClause) {
                    // ignore mock clauses
                    continue;
                }
                if (child.isLeftRecursive(rule, seen)) {
                    return true;
                }
                return false;
            }

            return false;
        }
    };

    static OrderedChoice = class OrderedChoice extends Clause {
        /**
         * Represents a list of alternative clauses which is attempted in order until one
         * succeeds. If no clauses succeed, the parse fails.
         * 
         * In a grammar, choices are indicated by a pipe symbol (or in classic PEGs the forward slash symbol /)
         * 
         * @param  {...Clause} clauses 
         */
        constructor(...clauses) {
            super({
                terminal: false,
                children: clauses,
                literal: "(" + clauses.join(" | ") + ")",
                prepare: (grammar, rule) => {
                    for (const clause of clauses) {
                        /*
                        if (!(clause.prepare instanceof Function)) {
                            console.log(clause, clause.prepare);
                        }
                        */
                        clause.prepare(grammar, rule);
                    }
                },
                setup: (grammar, rule) => {
                    for (const clause of clauses) {
                        clause.setup();
                    }
                }
            }, clauses);

        }

        parseFunction(state) {
            const offset = state.offset;
            if (this.data.hasLeftRecursion) {
                const result = this.lrParse(state);
                if (result === null) {
                    state.offset = offset;
                    return ParseState.MISS;
                }
                return result.result;
            }
            const startOffset = state.offset;
            try {
                for (const clause of this.children) {
                    const result = clause.parse(state);
                    if (result === ParseState.MISS) {
                        state.offset = startOffset;
                        continue;
                    }
                    return result;
                }
                return ParseState.MISS;
            } catch (e) {
                if (e instanceof LeftRecursionError) {
                    if (this.rule.clause !== this) {
                        throw new Error("Left recursion should only be possible for the root clause in a rule");
                    }
                    this.setupLeftRecursion();
                    return this.parseFunction(state);
                }
                throw e;
            }
        }

        setupLeftRecursion() {
            this.data.hasLeftRecursion = true;
            let precedence = (this.children.length - 1) * 100;
            for (const clause of this.children) {
                const pragmas = clause.pragmas;
                if ("assoc" in pragmas) {
                    if (['left','right'].indexOf(pragmas.assoc) < 0) {
                        throw new Error("Invalid pragma assoc=" + pragmas.assoc + " in rule " + clause.rule.name);
                    }
                } else {
                    pragmas.assoc = "left";
                }
                if ("precedence" in pragmas) {
                    pragmas.precedence = Number(pragmas.precedence);
                    precedence = pragmas.precedence;
                } else {
                    pragmas.precedence = precedence;
                }
                this.data.prepared.push(new PreparedClause(clause, clause.isLeftRecursive(this.rule), precedence))
                precedence -= 100;
            }
            this.data.prepared.sort((a, b) => {
                return b.precedence - a.precedence;
            });
        }

        /**
         * 
         * @param {ParseState} state 
         * @param {number} floor 
         * @param {boolean} rightAssociative 
         * @returns {Result | null}
         */
        lrParse(state, floor=0, rightAssociative=false) {
            const offset = state.offset;

            /**
             * Obtain a start result
             */
            let start = null;
            for (const prep of this.data.prepared) {
                if (prep.isLeftRecursive) {
                    continue;
                }
                start = this.parsePreparedClause(state, prep, null);
                if (start !== null) {
                    break;
                }
            }

            if (start === null) {
                state.offset = offset;
                return null;
            }

            /**
             * Using the start result, process any recursive clauses
             */
            let rightOffset = state.offset;
            for(;;) {
                let newStart = null;
                for (const prep of this.data.prepared) {
                    if (!prep.isLeftRecursive) {
                        continue;
                    }
                    if (prep.precedence === floor && rightAssociative) {

                    } else if (prep.precedence <= floor) {
                        // skip this prepared clause since it's not included in the allowed precedences
                        continue;
                    }
                    newStart = this.parsePreparedClause(state, prep, start);
                    if (newStart !== null) {
                        break;
                    }
                }
                if (newStart === null) {
                    break;
                }
                rightOffset = state.offset;
                start = newStart;
            }
            return start;
        }

        /**
         * 
         * @param {ParseState} state 
         * @param {PreparedClause} prep 
         * @param {Result} start 
         * @returns {Result|null}
         */
        parsePreparedClause(state, prep, start=null) {
            const floor = prep.precedence;
            const offset = state.offset;
            if (!prep.isLeftRecursive) {
                const result = prep.clause.parse(state);
                if (result === ParseState.MISS) {
                    return null;
                }
                return new Result(this.rule.name, result, prep.precedence);
            } else if (start !== null) {
                const oldParseFilter = parseFilter;
                parseFilter = (state, clause) => {
                    if (clause instanceof Clause.Call && clause.data.target.name === start.ruleName) {
                        if (state.offset === offset) {
                            // this is the leftmost clause
                            return start.result;
                        } else {
                            let match = this.lrParse(state, prep.precedence, prep.clause.pragmas.assoc === "right");
                            if (match !== null) {
                                return match.result;
                            }
                        }
                    }

                    return clause.parseFunction(state);
                };
                const result = prep.clause.parse(state);
                parseFilter = oldParseFilter;
                if (result === ParseState.MISS) {
                    state.offset = offset;
                    return null;
                }
                return new Result(this.rule.name, result, prep.precedence);
            } else {
                //console.log(prep, start);
                throw new Error("Tried parsePreparedClause without start on a left recursive clause");
            }
        }

        data = {
            /** @type {boolean} */
            hasLeftRecursion: false,
            /** @type {PreparedClause[]} */
            prepared: [],
        }


        /**
         * May this clause end up at this rule without making any progress?
         * 
         * @param {Clause} rule
         * @param {Clause[]} seen
         * @returns {boolean}
         */
        isLeftRecursive(rule, seen=[]) {
            if (seen.indexOf(this) >= 0) {
                // found left recursion which does not lead to rule
                return false;
            }
            seen.push(this);
            // Check each of the child clauses to see if they are left recursive
            for (const clause of this.children) {
                if (clause.isMockClause) {
                    continue;
                }
                if (clause.isLeftRecursive(rule, seen.slice(0))) {
                    return true;
                }
            }
            return false;
        }
    };

    static Call = class Call extends Clause {

        /**
         * Represents a call to another rule in the grammar.
         * 
         * In a grammar, these calls are indicated by the rule name.
         * 
         * @param {string} targetRuleName 
         */
        constructor(targetRuleName) {
            if (targetRuleName == ";") {
                throw new Error("Unknown ;")
            }
            super({
                terminal: false,
                literal: targetRuleName,
                prepare: (grammar, rule) => {
                    /**
                     * @type {Clause}
                     */
                    const targetRule = grammar.getRule(targetRuleName);
                    if (!targetRule) {
                        throw new Error("Grammar refers to undefined rule '" + targetRuleName + "'");
                    }
                    if (this.options.returnCount > 0) {
                        this.options.returnCount = targetRule.clause.options.returnCount;
                    }
                    if (!this.options.notProgressingParse) {
                        this.options.notProgressingParse = targetRule.clause.options.notProgressingParse;
                    }
                    this.data.target = targetRule;
                },
                setup: (grammar, rule) => {
                },
            });
        }

        data = {
            /**
             * @type {Clause}
             */
            target: undefined
        }

        parseFunction(state) {
            return this.data.target.parse(state);
        }

        /**
         * May this clause end up at this clause without making any progress?
         * 
         * @param {Clause} rule
         * @param {Clause[]} seen
         * @returns {boolean}
         */
        isLeftRecursive(rule, seen=[]) {
            if (this.data.target === rule) {
                return true;
            }
            if (seen.indexOf(this) >= 0) {
                return false;
            }
            seen.push(this);
            return this.data.target.clause.isLeftRecursive(rule, seen);
        }
    }

    static EOF = class EOF extends Clause {

        /**
         * End Of File symbol $ or __EOF
         * 
         * Represents a match at the end of the input stream.
         */
        constructor() {
            super({
                terminal: true,
                returnCount: 0,
                notProgressingParse: true,
                literal: '$'
            });
        }

        parseFunction(state) {
            return state.offset === state.length ? ParseState.HIT : ParseState.MISS;
        }
    }

    static SOF = class SOF extends Clause {
        /**
         * Start Of File symbol ^ or __SOF
         * 
         * Represents a match at the beginning of input stream.
         */
        constructor() {
            super({
                terminal: true,
                returnCount: 0,
                notProgressingParse: true,
                literal: '^'
            });
        }

        parseFunction(state) {
            return state.offset === 0 ? ParseState.HIT : ParseState.MISS;
        }
    }

    static Commit = class Commit extends Clause {

        /**
         * Commit symbol @ or __COMMIT (CPEG extension)
         * 
         * Represents parse commit action. When this token is seen, everything that
         * has been parsed so far is assumed to be correctly parsed at this point.
         * 
         * If parsing fails, it will be considered a syntax error in the input stream.
         */
        constructor() {
            super({
                terminal: true,
                returnCount: 0,
                notProgressingParse: true,
                literal: '@',
                isMockClause: true,
            });
        }

        parseFunction(state) {
            state.commit();
            return ParseState.HIT;
        }
    }

    static RegularExpression = class RegularExpression extends Clause {
        /**
         * Terminal Regular Expression /pattern/ (CPEG extension)
         * 
         * Matches a regular expression with the input string. Returns the matched string.
         * 
         * @param {RegExp} regexp 
         */
        constructor(regexp) {
            super({
                terminal: true,
                literal: "" + regexp,
                prepare: (grammar, rule) => {
                    this.data.regexp = new RegExp(regexp, "y");
                }
            });
        }

        data = {
            /**
             * The Regexp instance
             * @type {RegExp}
             */
            regexp: null
        }

        parseFunction(state) {
            const regexp = this.data.regexp;
            regexp.lastIndex = state.offset;
            const result = regexp.exec(state.source);
            if (result !== null) {
                state.offset = regexp.lastIndex;
                return result[0];
            }
            return ParseState.MISS;
        }
    }

    static Text = class Text extends Clause {

        /**
         * Terminal string "pattern"
         * 
         * Matches a literal string with the input string. Returns the matched string.
         * 
         * @param {string} text 
         */
        constructor(text) {
            super({
                terminal: true,
                literal: JSON.stringify(text),
            });
            const textString = text;
            const textLength = text.length;
            Object.defineProperty(this, 'parseFunction', {
                writable: true,
                value: (state) => {
                    if (state.source.substring(state.offset, state.offset + textLength) === textString) {
                        state.offset += textLength;
                        return textString;
                    }
                    return ParseState.MISS;
                }
            });
        }
    }

    static Epsilon = class Epsilon extends Clause {

        /**
         * Epsilon symbol Ε, ε, € or __EPSILON (CPEG extension)
         * 
         * Always succeeds at parsing but does not consume any input or add any values to the
         * result.
         * 
         * For convenience the Euro symbol € can be used since it is easier to write than the
         * epsilon characters which is not the same as the latin characters "E" or "e".
         */
        constructor() {
            super({
                terminal: true,
                returnCount: 0,
                notProgressingParse: true,
                literal: 'ε',
            });
        }

        parseFunction(state) {
            return ParseState.HIT;            
        }
    }

    /**
     * @param {ParseState} state 
     */
    parse(state) {
        if (this.activeIndex > 0 && this.activeOffsets[this.activeIndex-1] === state.offset) {
            throw new LeftRecursionError("The rule " + this.rule.name + ": " + this.rule.clause.toString() + " is left recursive and does not support left recursion", state.offset);
        }
        this.activeOffsets[this.activeIndex++] = state.offset;

        let parseFunction = this.parseFunction.bind(this);

        if (parseFilter !== null) {
            parseFunction = (state) => {
                return parseFilter(state, this);
            }
        }

        this.stats.invocations++;
        state.beginTrace(this);
        const result = parseFunction(state);
        if (result instanceof Result) { // TODO clean up and remove
            throw new Error("Parse function " + this.constructor.name + "::parseFunction() returned Result instance");
        }
        state.endTrace(this, result);
        this.activeIndex--;
        if (result !== ParseState.MISS && this.options.processor) {
            return (this.options.processor)(result, state);
        }
        return result;
    }

    toString() {
        return this.predicate + this.literal + this.operator;
    }
    
    
    /**
     * Return a reference to the parent clause by searching the rule clause
     * tree for a clause which has this clause as a child.
     * 
     * @type {Clause|null}
     */
    get parent() {
        if (!this.rule || this.rule.clause == this) {
            // root clauses has no parent
            return null;
        }
        if (this.data._parent) {
            return this.data._parent.deref();
        }
        // must traverse the rule hierarchy to find the ancestor
        /**
         * @param {Clause} parent 
         * @returns {Clause|null}
         */
        const search = (parent) => {
            if (!parent.children) {
                return null;
            }
            for (const child of parent.children) {
                if (child === this) {
                    return parent;
                }
                const candidate = search(child);
                if (candidate) {
                    return candidate;
                }
            }
            return null;
        }

        const parent = search(this.rule.clause);
        this.data._parent = new WeakRef(parent);
        return parent;
    }

    /**
     * @type {ClauseDescription|null}
     */
    get description() {
        return this.options.description;
    }
    set description(description) {
        if (typeof description !== "string") {
            throw new Error("Description must be a string");
        }
        this.options.description = description;
    }

    /**
     * Helper function for setting the description with method chaining.
     * 
     * @param {string} desc 
     * @returns {this}
     */
    setDescription(desc) {
        this.description = desc;
        return this;
    }

    /**
     * @type {{[pragma: string]: any}}
     */
    get pragmas() {
        return this.options.pragmas;
    }

    setPragma(pragma, value) {
        this.options.pragmas[pragma] = value;
        return this;
    }

    /**
     * @type {(value: any, state: ParseState) => any} processor 
     */
    get processor() {
        return this.options.processor;
    }
    set processor(processor) {
        this.options.processor = processor;
    }
    /**
     * Set a function which will process the parse result before
     * it is returned to the caller.
     * 
     * @param {(value: any, state: ParseState) => any} processor 
     * @returns {this}
     */
    setProcessor(processor) {
        this.processor = processor;
        return this;
    }

    /**
     * Is this a terminal clause?
     * 
     * @type {boolean}
     */
    get isTerminal() {
        return this.options.terminal;
    }

    get isNonTerminal() {
        return !this.options.terminal;
    }

    /**
     * How many values does this clause return to the result?
     * 
     * @type {number}
     */
    get returnCount() {
        return this.options.returnCount;
    }

    get isProvidingValues() {
        throw new Error("Don't use");
        return !this.isNotProvidingValues;
    }

    /**
     * Does the clause make parsing progress?
     */
    get isNotProgressingParse() {
        if (this.options.notProgressingParse instanceof Lazy) {
            return this.options.notProgressingParse = this.options.notProgressingParse.value;
        }
        return this.options.notProgressingParse;
    }
    get isProgressingParse() {
        return !this.isNotProgressingParse;
    }

    /**
     * Is this a mock clause which does not actually parse things, such as the @ commit clause?
     */
    get isMockClause() {
        return this.options.isMockClause;
    }

    /**
     * Will this clause succeed in any case
     */
    get isOptional() {
        if (this.options.operator === "*" || this.options.operator === "?") {
            return true;
        }
        return false;
    }

    /**
     * May this clause repeat itself (via the + or * operator)
     * 
     * @type {boolean}
     */
    get isRepeating() {
        if (this.options.operator === "*" || this.options.operator === "+") {
            return true;
        }
        return false;
    }

    /**
     * Using the ! predicate?
     * 
     * @type {boolean}
     */
    get isNegated() {
        return this.options.predicate === "!";
    }

    /**
     * Using the ... predicate?
     * 
     * @type {boolean}
     */
    get isDestructuring() {
        return this.options.predicate === "...";
    }

    /**
     * Using the & predicate?
     * 
     * @type {boolean}
     */
    get isLookAhead() {
        return this.options.predicate === "&";
    }

    /**
     * Using the - predicate?
     * 
     * @type {boolean}
     */
    get isDiscard() {
        return this.options.predicate === "-";
    }

    get predicate() {
        return this.options.predicate;
    }

    get literal() {
        return this.options.literal;
    }

    get operator() {
        return this.options.operator;
    }

    /**
     * The Discard Predicate - (CPEG extension)
     * 
     * Parsing must succeed and the parse offset may change, but the result of the parse is discarded (not added to the result)
     * 
     * @returns {this}
     */
    discard() {
        if (this.options.predicate !== "") {
            throw new Error(`The clause already has the '${this.options.predicate}' predicate`)
        }
        const parseFunction = this.parseFunction.bind(this);

        this.parseFunction = (state) => {
            const result = parseFunction(state);
            if (result !== ParseState.MISS) {
                return ParseState.HIT;
            }
            return result;
        };
        this.options.returnCount = 0;
        return this;
    }

    /**
     * The Destructuring Predicate ... (CPEG extension)
     * 
     * The values from this clause should be appended to the parent sequence
     * 
     * @returns {this}
     */
    destructure() {
        if (this.options.predicate !== "") {
            throw new Error(`The clause already has the '${this.options.predicate}' predicate`)
        }
        this.options.predicate = "...";
        return this;
    }

    /**
     * The Lookahead Predicate &
     * 
     * Parsing must succeed, but parse offset does not change and the result of the parse is not added to the result
     * 
     * @returns {this}
     */
    lookahead() {
        if (this.options.predicate !== "") {
            throw new Error(`The clause already has the '${this.options.predicate}' predicate`)
        }
        const parseFunction = this.parseFunction.bind(this);
        this.parseFunction = (state) => {
            const startOffset = state.offset;
            const result = parseFunction(state);
            state.offset = startOffset;

            return result !== ParseState.MISS ? ParseState.HIT : ParseState.MISS;
        }
        this.options.returnCount = 0;
        this.options.notProgressingParse = true;
        return this;

    }

    /**
     * The Not Predicate !
     * 
     * Failing to parse this clause will indicate success and allow parsing to resume. The parse offset will not change.
     * 
     * @returns {this}
     */
    not() {
        if (this.options.predicate !== "") {
            throw new Error(`The clause already has the '${this.options.predicate}' predicate`)
        }
        // predicates can't be mixed with other predicates
        const parseFunction = this.parseFunction.bind(this);
        this.parseFunction = (state) => {
            const startOffset = state.offset;
            const result = parseFunction(state);
            state.offset = startOffset;
            return result !== ParseState.MISS ? ParseState.MISS : ParseState.HIT;
        };
        this.options.predicate = "!";
        this.options.returnCount = 0;
        this.options.notProgressingParse = true;
        return this;
    }

    /**
     * The Optional Operator ?
     * 
     * If unable to parse this token, a `null` value is added to the result and parsing continues.
     * 
     * @returns {this}
     */
    optional() {
        if (this.options.operator !== "") {
            throw new Error(`The clause already has the '${this.options.operator}' operator`)
        }
        this.options.operator = "?";
        const parseFunction = this.parseFunction.bind(this);
        this.parseFunction = (state) => {
            const result = parseFunction(state);
            if (result === ParseState.MISS) {
                // returning null because we need to take a slot from the result sequence since the clause was optional
                return null;
            }
            return result;
        };
        return this;
    }

    /**
     * The Kleene Star *
     * 
     * Parse zero or more instances of the clause. Parsing never fails, and an array is always added to the result.
     * 
     * @returns {this}
     */
    zeroOrMore() {
        if (this.options.operator !== "") {
            throw new Error(`The clause already has the '${this.options.operator}' operator`)
        }
        this.options.operator = "*";
        const parseFunction = this.parseFunction.bind(this);
        this.parseFunction = (state) => {
            const results = [];
            for (;;) {
                const result = parseFunction(state);
                if (result === ParseState.MISS) {
                    return results;
                } else if (result !== ParseState.HIT) {
                    results.push(result);
                }
            }
        };
        return this;
    }

    /**
     * The Kleene Cross +
     * 
     * Parse one or more instances of the clause. If no matches are found, the parsing backtracks.
     * 
     * @returns {this}
     */
    oneOrMore() {
        if (this.options.operator !== "") {
            throw new Error(`The clause already has the '${this.options.operator}' operator`)
        }
        this.options.operator = "+";
        const parseFunction = this.parseFunction.bind(this);
        this.parseFunction = (state) => {
            const results = [];
            for (;;) {
                const result = parseFunction(state);
                if (result === ParseState.MISS) {
                    if (results.length === 0) {
                        return ParseState.MISS;
                    }
                    return results;
                } else if (result !== ParseState.HIT) {
                    results.push(result);
                }
            }
        };
        return this;        
    }

    /**
     * Print a debug name for this clause, which includes the rule name and the path
     * 
     * @returns {string}
     */
    get debugName() {
        /**
         * 
         * @param {Clause[]} path 
         * @param {Clause} next
         */
        const search = (path, index, next) => {
            path.push({index: index, clause: next});
            if (next === this) {
                return path;
            }

            if (next.children) {
                for (const [i, child] of next.children.entries()) {
                    const result = search(path, i, child);
                    if (result) {
                        return result;
                    }
                }    
            }
            // no match here
            path.pop();
            return null;
        }
        /**
         * @type {{index: number, clause: Clause}[]}
         */
        const path = search([], null, this.rule.clause);
        const root = path.shift();
        let debugName = this.rule.name + "(" + this.rule.clause.constructor.name + ")" + (path[0] ? "[" + path[0].index + "]" : "") + ": ";
        const parts = [];
        for (const [o, part] of path.entries()) {
            if (path[o+1]) {
                parts.push(`${part.clause.constructor.name}[${path[o+1].index}]`);
            } else {
                parts.push(part.clause.constructor.name);
            }
        }
        return "`" + debugName + (parts.length === 0 ? "" : ": " + parts.join("->")) + "`";
    }

    /**
     * Invoked as soon as the clause has been added to a rule.
     * 
     * @param {Clause} rule 
     */
    init(rule) {
        //console.log("init phase for", this.toString());
        this.rule = rule;
        if (this.children !== null) {
            for (const child of this.children) {
                /*
                if (child === null) {
                    console.log(this.toString());
                }
                */
                child.init(rule);
            }    
        }
    }

    /**
     * In this phase, clauses can set up relationships/references to other clauses 
     * but they should not query the other clauses for information they need.
     * 
     * @private
     * @param {Grammar} grammar 
     * @param {Clause} rule 
     */
    prepare(grammar, rule) {
        //console.log("prepare phase for", this.toString());
        this.prepareCalled = true;
        this.grammar = grammar;
        if (this.options.prepare) {
            this.options.prepare(grammar, rule);
        }
    }

    /**
     * Run the setup phase. In this phase the clauses can query other clauses and perform 
     * things like optimizations.
     * 
     * @param {*} grammar 
     * @param {*} rule 
     */
    setup(grammar, rule) {
        //console.log("setup phase for", this.toString());
        this.setupCalled = true;
        if (this.options.setup) {
            this.options.setup(grammar, rule);
        }
    }

    /**
     * Visit all children in pre order. Clauses with multiple paths will yield arrays to indicate the alternative paths.
     * 
     * @yields {Clause|Clause[]}
     */
    *visitPreOrder() {
        yield this;
        if (this.children !== null) {
            for (const child of this.children) {
                yield* child.visitPreOrder();
            }
        }
    }

    /**
     * Visit all children leafs first, left to right. Children with multiple paths will yield arrays to indicate the alternative paths.
     * 
     * @yields {Clause|Clause[]}
     */
    *visitPostOrder() {
        if (this.children !== null) {
            for (const child of this.children) {
                yield* child.visitPostOrder();
            }
        }
        yield this;
    }

    /**
     * Will this clause always end up at the same offset without making any progress?
     * 
     * @param {Clause} rule
     * @param {Clause[]} seen
     * @returns {boolean}
     */
    isLeftRecursive(rule, seen=[]) {
        // non-terminals should override this method
        return false;
    }

    [util.inspect.custom]() {
        return debugObject(this.constructor.name, {
            term: this.toString()
        });
    }
    /**
     * Create a custom parse function.
     * 
     * ### parseFunction
     * 
     * For adding custom parsing logic to the grammar. A parse function will receive an instance of `ParseState` and is responsible for updating
     * its `offset` property as needed.
     * 
     * Special return values:
     * 
     * - ParseState.HIT is a symbol which indicates a successful match, but does not add anything to the result.
     * - ParseState.MISS is a symbol which indicates a failed match. The `ParseState.offset` value MUST remain unmodified.
     * 
     * Any other return value is passed back to the invoking function or becomes the final result of the parse.
     * 
     * State information can be recorded in the `ParseState` object as needed as long as the key begins with a dollar sign $, e.g.
     * `state.$customData = someValue;`. Failing to prefix the key may cause unpredictable behavior.
     * 
     * ### options
     * 
     * The options object provides metadata about the parse function which may be used by other clauses that invoke your parse
     * function:
     * 
     * - `notProgressingParse=true` The parse function will not capture any input. The `offset` will remain the same.
     * - `notProvidingValues=true` The parse function may capture input, but it will only return `ParseState.HIT` or `ParseState.MISS`.
     * - `literal` A string representing how this clause would be written in a text grammar.
     * 
     * @param {any[]} Initial constructor args
     * @param {(state: ParseState) => (any)} parseFunction Function returns ParseState.HIT or ParseState.MISS or a value to add to the parse tree.
     * @param {ClauseOptions} options Is this a capturing clause
     * @param {Object} data Any data properties that the clause needs to store should be declared
     * @param {Clause[]|null} children Any child clauses
     */
    constructor(options, children=null) {
        // these properties are sealed here because they should be declared on the class
        this.prepareCalled = false;
        this.setupCalled = false;

        // Optimization; ensure that this.options always has the same shape and property order
        /**
         * @private
         * @type {ClauseOptions}
         */
        Object.assign(this.options, options);

        /**
         * The rule which this clause belongs to
         * 
         * @type {Clause}
         */
        this.rule = null;

        /**
         * The grammar which this clause belongs to
         * 
         * @type {Grammar}
         */
        this.grammar = null

        //console.log("adding", children ? children.length : "no","children for", this.constructor.name);
        this.children = children;

        Object.seal(this.options);
        Object.seal(this.children);
    }

    /**
     * Implement the parse function
     * @internal
     * @param {ParseState} state 
     */
    parseFunction(state) {
        throw new Error(this.constructor.name + "::parseFunction() not implemented");
    }

    /**
     * Options and properties from the extending class
     */
    options = {
        /** @type {boolean} */
        terminal: false,
        /** @type {number} */
        returnCount: 1,
        /** @type {boolean} */
        notProgressingParse: false,
        /** @type {boolean} */
        isMockClause: false,
        /** @type {"!"|"&"|"..."|"-"|""} */
        predicate: "",
        /** @type {"?"|"+"|"*"|"{}"|""} */
        operator: "",
        processor: null,
        /** @type {{[pragma: string]: any}} */
        pragmas: {},
        prepare: null,
        setup: null,
        literal: "",
        description: "",
    };

    children = null;

    /**
     * Internal data for the clause
     * @private
     */
    data = {
    }

    /**
     * The start offset of any ongoing parses
     * 
     * @private
     * @type {number[]}
     */
    activeOffsets = [];
    /**
     * The index of the currently active offset
     * 
     * @private
     */
    activeIndex = 0;

    /**
     * Statistics from the parsing process
     */
    stats = {
        /**
         * Counts how many times this clause was invoked
         */
        invocations: 0,

        /**
         * Counts how many successful results this clause yielded
         */
        successfulResults: 0,

        /**
         * Counts how many failed results this clause yielded
         * 
         * @type {number}
         */
        failedResults: 0,
    };
}

