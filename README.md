CPEG
====

The Charm PEG parser is a fast implementation of PEG parsing with support for left-recursion.

Usage
-----

Defining a simple JSON parser

    import CPEG from "cpeg";

    const parser = new CPEG(`
        JSON    : Value
        Value   : Number | String | Boolean | Null | Object | Array ;
        Number  : /(0|[1-9][0-9]*)(\.[0-9]+)?/ ;
        String  : /"(\\[\"]|[^"])*"/ ;
        Boolean : /true|false/ ;
        Null    : /null/ ;
        Object  : "{" _ Members? "}" ;
        Members : String _ ":" _ Value _ ("," _ String _ ":" _ Value _)*
        Array   : "[" _ Values? "]" ;
        Values  : Value _ ("," _ Value)*
    `);

    const json = parser.parse("[1,2,3]");