import Parser from "./parser.mjs";
import Clause from "./clause.mjs";
import ParseState from "./parse-state.mjs";
import Rule from "./rule.mjs";

/**
 * Represents a BNF style grammar consisting of Rule instances.
 */
export default class Grammar {

    /**
     * @extends {Parser<void>}
     */
    static Parser = class GrammarParser extends Parser {

        /**
         * Construct a CPEG grammar parser
         */
        constructor() {
            /**
             * @param {string} name 
             */
            const c = (name) => new Clause.Call(name);

            /**
             * 
             * @param {string} name 
             * @param {Clause} clause
             * @param {(result: any, state: ParseState) => any} processor
             */
            const r = (name, clause) => new Rule(name, clause);

            /**
             * @param  {...Clause} clauses 
             */
            const s = (...clauses) => new Clause.Sequence(...clauses);

            /**
             * @param {string} text 
             * @returns 
             */
            const t = (text) => new Clause.Text(text);

            /**
             * @param {RegExp} rx 
             */
            const rx = (rx) => new Clause.RegularExpression(rx);

            /**
             * @param  {...Clause} clauses
             */
            const oc = (...clauses) => new Clause.OrderedChoice(...clauses);

            const SOF = () => new Clause.SOF();
            const EOF = () => new Clause.EOF();

            //
            // GRAMMAR
            // 
            //  Grammar     : ^ _ Rules $
            //  Rules       : Rule+
            //  Rule        : RuleName ":" _ Clauses ";" _
            //  RuleName    : /[a-zA-Z][a-zA-Z0-9]*/
            //  Clauses     : Clause+
            //  Clause      : Predicate? ClauseName Operator?
            //  Predicate   : /[!&]/
            //  Operator    : /[+*?]/
            //  ClauseName  : /[a-zA-z][a-zA-Z0-9]|\^|\$/
            //  _           : /\s*/
            //

            const grammar = new Grammar(
                // Grammar      :   ^_ Rules $
                r("Grammar",        s(SOF(), c("_"), c("Rules"), EOF()).setDescription("CPEG grammar").setProcessor((rules) => {
                    return new Grammar(...rules);
                })),
                // Rules        :   Rule+
                r("Rules",          c("Rule").oneOrMore().setDescription("rule definitions")),
                // Rule         :   RuleName ":" _ Clauses (Macro | ";") _ ;
                r("Rule",           s(
                    c("RuleName"),
                    c("_"), 
                    c("StringLit").optional(), 
                    c("_"),
                    t(":").discard(),
                    c("_"), 
                    c("Clauses"), 
                    c("_"), 
                    t(";").discard(),
                    c("_")
                ).setDescription("rule definition").setProcessor((m) => {
                    const result = new Rule(m[0], m[2]);
                    if (m[1]) {
                        result.clause.setDescription(m[1]);
                    }
                    return result;
                })),
                // RuleName     :   /[a-zA-Z][a-zA-Z0-9]*/
                r("RuleName",       rx(/[a-zA-z_][a-zA-Z0-9_]*/).setDescription("rule name")),
                // Clauses      :   Clause+ ("|" _ Clause+)*
                r("Clauses",        s(
                    c("Clause").oneOrMore(), 
                    c("Pragma").zeroOrMore(), 
                    c("Macro").optional(), 
                    c("_"), 
                    s(
                        t("|").discard(), 
                        c("_"), 
                        c("Clause").oneOrMore(), 
                        c("Pragma").zeroOrMore(),  
                        c("Macro").optional(), 
                        c("_")
                        ).zeroOrMore()
                    ).setDescription("list of clauses").setProcessor((m) => {
                    /**
                     * 
                     * @param {Clause[]} v clauses
                     * @param {any[]} p pragma defs
                     * @param {(value: any)=>any} m macro
                     */
                    const sequencify = (v, p, m) => {
                        let result;
                        if (v.length > 1) {
                            result = new Clause.Sequence(...v);
                        } else {
                            result = v[0];
                        }
                        if (m) {
                            result.setProcessor(m);
                        }
                        for (const pragma of p) {
                            result.setPragma(pragma[0], pragma[1]);
                        }
                        return result;
                    };

                    if (m[3].length > 0) {
                        // Ordered Choice
                        const choices = [ sequencify(m[0], m[1], m[2]) ];
                        m[3].forEach((choiceDef) => {
                            choices.push(sequencify(choiceDef[0], choiceDef[1], choiceDef[2]));
                        });
                        return new Clause.OrderedChoice(...choices);
                    } else {
                        return sequencify(m[0], m[1], m[2]);
                    }
                })),
                // Clause       :   Predicate? (ClauseName | StringLit | RegexLit | SubSequence) Operator? _
                r("Clause",         s(
                    c("Predicate").optional(),
                    oc(
                        c("ClauseName"), 
                        c("StringClause"), 
                        c("RegexLit"), 
                        c("SubSequence")
                    ), 
                    c("Operator").optional(), c("_")
                ).setProcessor((m) => {
                    const predicate = m[0];
                    /**
                     * @type {Clause}
                     */
                    let clause = m[1];
                    const operator = m[2];

                    if (operator === "?") {
                        clause = clause.optional();
                    } else if (operator === "*") {
                        clause = clause.zeroOrMore();
                    } else if (operator === "+") {
                        clause = clause.oneOrMore();
                    } else if (operator === null) {
                    } else {
                        throw "Unknown operator " + operator
                    }

                    if (predicate === "!") {
                        clause = clause.not();
                    } else if (predicate === "&") {
                        clause = clause.lookahead();
                    } else if (predicate === "-") {
                        clause = clause.discard();
                    } else if (predicate === "...") {
                        clause = clause.destructure();
                    } else if (predicate === null) {

                    } else {
                        throw "Unknown predicate " + predicate;
                    }

                    return clause;
                })),
                // SubSequence  :   "(" _ Clauses ")"
                r("SubSequence",    s(t("(").discard(), c("_"), c("Clauses"), t(")").discard())),
                // StringLit    :   /"(\\[\"]|[^"])*"/
                r("StringLit",      rx(/"(\\[\"]|[^"])*"/).setDescription("string").setProcessor((s) => {
                    return JSON.parse(s);
                })),
                r("StringClause",   rx(/"(\\[\"]|[^"])*"/).setDescription("text clause").setProcessor((s) => {
                    /**
                     * @type {string}
                     */
                    let text = s;
                    text = text.replaceAll(/\\\\|\\"/g, (match) => { return {"\\\\": "\\\\", "\\\"": '"'}[match]; });
                    text = text.substring(1, text.length - 1);
                    return new Clause.Text(text);
                })),
                // RegexLit     :   /\/(\\[\\\/]|[^\/])*\//
                r("RegexLit",       rx(/\/(\\[\\\/]|[^\/])*\/[a-z]*/).setDescription("regular expression").setProcessor((r) => {
                    let parts = r.split("/").slice(1)
                    const flags = parts.pop();
                    const regex = new RegExp(parts.join("/"), flags);
                    return new Clause.RegularExpression(regex);
                })),
                // Predicate    :   /[!&]/
                r("Predicate",      rx(/[!&-]|\.\.\./).setDescription("predicate")),
                // Operator     :   /[+*?]/
                r("Operator",       rx(/[+*?]/).setDescription("operator")),
                // ClauseName   :   /[a-zA-z][a-zA-Z0-9]|\^|\$|€/
                r("ClauseName",     rx(/[a-zA-z_][a-zA-Z0-9_]*|\^|\$(?![a-zA-Z_])|€|Ε|ε/).setDescription("rule name or symbol").setProcessor((s) => {
                    switch (s) {
                        case '__SOF':
                        case '^':
                            return new Clause.SOF();

                        case '__EOF':
                        case '$':
                            return new Clause.EOF();

                        case '__COMMIT':
                        case '@':
                            return new Clause.Commit();

                        case '__EPSILON':
                        case 'Ε':
                        case 'ε':
                        case '€':
                            return new Clause.Epsilon();
                    }
                    return new Clause.Call(s)
                })),
                r("Pragma",     s(t("%").discard(), rx(/[a-z]+/), t("=").discard(), rx(/[a-zA-Z0-9_\-]+/), c("_")).setProcessor((values, state) => {
                    return values;
                })),
                r("Macro",      rx(/%%[0-9]+%%/).setDescription("macro function").setProcessor((v, state) => {
                    const macroKey = Number(v.substring(2, v.length - 2));
                    if (macroKey in state.arguments) {
                        return state.arguments[macroKey];
                    }
                    throw new Error("Expecting argument #" + macroKey);
                })),
                // _            :   /\s*/
                r("_",              oc(
                    c("Whitespace"),
                    c("Comment"),
                ).zeroOrMore().discard()),
                // __           :   /\s+/
                r("__",             oc(c("Whitespace"), c("Comment")).oneOrMore().discard()),
                r("Whitespace",     rx(/\s+/).discard().setDescription("whitespace")),
                r("Comment",        rx(/\/\*([^*]+|\*(?!\/))*\*\//).discard())
            );

            super(grammar);
        }

        /**
         * @param {string} grammar A grammar in BNF form
         * @param {Function[]} args Arguments can be injected in the grammar using `$0`, `$1` and so on.
         * @returns {Grammar}
         */
        parse(grammar, ...args) {
            return super.parse(grammar, ...args);
        }
    };

    /**
     * @param  {Rule[]} rules
     */
    constructor(...rules) {
        for (const rule of rules) {
            if (!(rule instanceof Rule)) {
                throw "Expected Rule instance, got1 " + rule;
            }
        }
        /**
         * @type {Rule[]}
         */
        this.rules = rules;

        this.goal = rules[0];

        /**
         * Callbacks that will be invoked after the prepare/setUp phase have completed for
         * all rules.
         * 
         * @private
         * @type {(()=>void)[]}
         */
        this.deferredTasks = [];

        // Run the setup stages
        this.prepare();        
        this.setUp();
    }

    /**
     * Run a callback immediately after the prepare or setup phase.
     * 
     * @param {()=>any} callback 
     */
    defer(callback) {
        this.deferredTasks.push(callback);
        return this;
    }

    /**
     * Run all deferred tasks
     * @private
     * @returns 
     */
    runDeferredTasks() {
        while (this.deferredTasks.length > 0) {
            this.deferredTasks.shift()();
        }
    }

    toString() {
        let result = "\n";
        for (const rule of this.rules) {
            result += rule.toString() + "\n";
        }
        return result;
    }

    /**
     * @param {string} name 
     * @returns {Rule|null}
     */
    getRule(name) {
        for (const rule of this.rules) {
            if (rule.name === name) {
                return rule;
            }
        }
        return null;
    }

    prepare() {
        this.rules.forEach((rule) => {
            rule.prepare(this);
        });
        this.runDeferredTasks();
    }

    setUp() {
        this.rules.forEach((rule) => {
            rule.setup(this);
        });
        this.runDeferredTasks();
    }
}